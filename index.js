const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
const spanGreetings = document.querySelector('.span-greetings');
const txtLastName = document.querySelector('#txt-last-name');



txtFirstName.addEventListener('keyup', (event)=>{
    spanFullName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('keyup', (event) =>{
    spanFullName.innerHTML = txtLastName.value;
})

const fullNameDisplay = () => {
    let firstName = txtFirstName.value
    let lastName = txtLastName.value
    let greeting=`Hi, welcome!`
    spanFullName.innerHTML = firstName + ' ' + lastName;
}
txtFirstName.addEventListener('keyup', fullNameDisplay);
txtLastName.addEventListener('keyup', fullNameDisplay);